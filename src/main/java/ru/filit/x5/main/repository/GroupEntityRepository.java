package ru.filit.x5.main.repository;

import ru.filit.x5.main.domain.GroupEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the GroupEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GroupEntityRepository extends JpaRepository<GroupEntity, Long> {

}
