package ru.filit.x5.main.repository;

import ru.filit.x5.main.domain.ClusterEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ClusterEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClusterEntityRepository extends JpaRepository<ClusterEntity, Long> {

}
