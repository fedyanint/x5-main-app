package ru.filit.x5.main.repository;

import ru.filit.x5.main.domain.ProductEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProductEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductEntityRepository extends JpaRepository<ProductEntity, Long> {

}
