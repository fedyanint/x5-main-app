package ru.filit.x5.main.service.impl;

import ru.filit.x5.main.service.ProductEntityService;
import ru.filit.x5.main.domain.ProductEntity;
import ru.filit.x5.main.repository.ProductEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductEntity}.
 */
@Service
@Transactional
public class ProductEntityServiceImpl implements ProductEntityService {

    private final Logger log = LoggerFactory.getLogger(ProductEntityServiceImpl.class);

    private final ProductEntityRepository productEntityRepository;

    public ProductEntityServiceImpl(ProductEntityRepository productEntityRepository) {
        this.productEntityRepository = productEntityRepository;
    }

    /**
     * Save a productEntity.
     *
     * @param productEntity the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProductEntity save(ProductEntity productEntity) {
        log.debug("Request to save ProductEntity : {}", productEntity);
        return productEntityRepository.save(productEntity);
    }

    /**
     * Get all the productEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProductEntity> findAll(Pageable pageable) {
        log.debug("Request to get all ProductEntities");
        return productEntityRepository.findAll(pageable);
    }

    /**
     * Get one productEntity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProductEntity> findOne(Long id) {
        log.debug("Request to get ProductEntity : {}", id);
        return productEntityRepository.findById(id);
    }

    /**
     * Delete the productEntity by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductEntity : {}", id);
        productEntityRepository.deleteById(id);
    }
}
