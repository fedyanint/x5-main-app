package ru.filit.x5.main.service;

import ru.filit.x5.main.domain.GroupEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link GroupEntity}.
 */
public interface GroupEntityService {

    /**
     * Save a groupEntity.
     *
     * @param groupEntity the entity to save.
     * @return the persisted entity.
     */
    GroupEntity save(GroupEntity groupEntity);

    /**
     * Get all the groupEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GroupEntity> findAll(Pageable pageable);

    /**
     * Get the "id" groupEntity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GroupEntity> findOne(Long id);

    /**
     * Delete the "id" groupEntity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
