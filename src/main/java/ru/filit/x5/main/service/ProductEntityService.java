package ru.filit.x5.main.service;

import ru.filit.x5.main.domain.ProductEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ProductEntity}.
 */
public interface ProductEntityService {

    /**
     * Save a productEntity.
     *
     * @param productEntity the entity to save.
     * @return the persisted entity.
     */
    ProductEntity save(ProductEntity productEntity);

    /**
     * Get all the productEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProductEntity> findAll(Pageable pageable);

    /**
     * Get the "id" productEntity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductEntity> findOne(Long id);

    /**
     * Delete the "id" productEntity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
