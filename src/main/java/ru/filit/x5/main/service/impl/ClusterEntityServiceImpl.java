package ru.filit.x5.main.service.impl;

import ru.filit.x5.main.service.ClusterEntityService;
import ru.filit.x5.main.domain.ClusterEntity;
import ru.filit.x5.main.repository.ClusterEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ClusterEntity}.
 */
@Service
@Transactional
public class ClusterEntityServiceImpl implements ClusterEntityService {

    private final Logger log = LoggerFactory.getLogger(ClusterEntityServiceImpl.class);

    private final ClusterEntityRepository clusterEntityRepository;

    public ClusterEntityServiceImpl(ClusterEntityRepository clusterEntityRepository) {
        this.clusterEntityRepository = clusterEntityRepository;
    }

    /**
     * Save a clusterEntity.
     *
     * @param clusterEntity the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClusterEntity save(ClusterEntity clusterEntity) {
        log.debug("Request to save ClusterEntity : {}", clusterEntity);
        return clusterEntityRepository.save(clusterEntity);
    }

    /**
     * Get all the clusterEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClusterEntity> findAll(Pageable pageable) {
        log.debug("Request to get all ClusterEntities");
        return clusterEntityRepository.findAll(pageable);
    }

    /**
     * Get one clusterEntity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClusterEntity> findOne(Long id) {
        log.debug("Request to get ClusterEntity : {}", id);
        return clusterEntityRepository.findById(id);
    }

    /**
     * Delete the clusterEntity by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClusterEntity : {}", id);
        clusterEntityRepository.deleteById(id);
    }
}
