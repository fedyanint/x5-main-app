package ru.filit.x5.main.service.impl;

import ru.filit.x5.main.service.GroupEntityService;
import ru.filit.x5.main.domain.GroupEntity;
import ru.filit.x5.main.repository.GroupEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link GroupEntity}.
 */
@Service
@Transactional
public class GroupEntityServiceImpl implements GroupEntityService {

    private final Logger log = LoggerFactory.getLogger(GroupEntityServiceImpl.class);

    private final GroupEntityRepository groupEntityRepository;

    public GroupEntityServiceImpl(GroupEntityRepository groupEntityRepository) {
        this.groupEntityRepository = groupEntityRepository;
    }

    /**
     * Save a groupEntity.
     *
     * @param groupEntity the entity to save.
     * @return the persisted entity.
     */
    @Override
    public GroupEntity save(GroupEntity groupEntity) {
        log.debug("Request to save GroupEntity : {}", groupEntity);
        return groupEntityRepository.save(groupEntity);
    }

    /**
     * Get all the groupEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GroupEntity> findAll(Pageable pageable) {
        log.debug("Request to get all GroupEntities");
        return groupEntityRepository.findAll(pageable);
    }

    /**
     * Get one groupEntity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<GroupEntity> findOne(Long id) {
        log.debug("Request to get GroupEntity : {}", id);
        return groupEntityRepository.findById(id);
    }

    /**
     * Delete the groupEntity by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete GroupEntity : {}", id);
        groupEntityRepository.deleteById(id);
    }
}
