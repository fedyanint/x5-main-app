package ru.filit.x5.main.service;

import ru.filit.x5.main.domain.ClusterEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ClusterEntity}.
 */
public interface ClusterEntityService {

    /**
     * Save a clusterEntity.
     *
     * @param clusterEntity the entity to save.
     * @return the persisted entity.
     */
    ClusterEntity save(ClusterEntity clusterEntity);

    /**
     * Get all the clusterEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClusterEntity> findAll(Pageable pageable);

    /**
     * Get the "id" clusterEntity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClusterEntity> findOne(Long id);

    /**
     * Delete the "id" clusterEntity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
