package ru.filit.x5.main.web.rest;

import ru.filit.x5.main.domain.GroupEntity;
import ru.filit.x5.main.service.GroupEntityService;
import ru.filit.x5.main.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.filit.x5.main.domain.GroupEntity}.
 */
@RestController
@RequestMapping("/api")
public class GroupEntityResource {

    private final Logger log = LoggerFactory.getLogger(GroupEntityResource.class);

    private static final String ENTITY_NAME = "mainGroupEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GroupEntityService groupEntityService;

    public GroupEntityResource(GroupEntityService groupEntityService) {
        this.groupEntityService = groupEntityService;
    }

    /**
     * {@code POST  /group-entities} : Create a new groupEntity.
     *
     * @param groupEntity the groupEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new groupEntity, or with status {@code 400 (Bad Request)} if the groupEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/group-entities")
    public ResponseEntity<GroupEntity> createGroupEntity(@RequestBody GroupEntity groupEntity) throws URISyntaxException {
        log.debug("REST request to save GroupEntity : {}", groupEntity);
        if (groupEntity.getId() != null) {
            throw new BadRequestAlertException("A new groupEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GroupEntity result = groupEntityService.save(groupEntity);
        return ResponseEntity.created(new URI("/api/group-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /group-entities} : Updates an existing groupEntity.
     *
     * @param groupEntity the groupEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated groupEntity,
     * or with status {@code 400 (Bad Request)} if the groupEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the groupEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/group-entities")
    public ResponseEntity<GroupEntity> updateGroupEntity(@RequestBody GroupEntity groupEntity) throws URISyntaxException {
        log.debug("REST request to update GroupEntity : {}", groupEntity);
        if (groupEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GroupEntity result = groupEntityService.save(groupEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, groupEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /group-entities} : get all the groupEntities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of groupEntities in body.
     */
    @GetMapping("/group-entities")
    public ResponseEntity<List<GroupEntity>> getAllGroupEntities(Pageable pageable) {
        log.debug("REST request to get a page of GroupEntities");
        Page<GroupEntity> page = groupEntityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /group-entities/:id} : get the "id" groupEntity.
     *
     * @param id the id of the groupEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the groupEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/group-entities/{id}")
    public ResponseEntity<GroupEntity> getGroupEntity(@PathVariable Long id) {
        log.debug("REST request to get GroupEntity : {}", id);
        Optional<GroupEntity> groupEntity = groupEntityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(groupEntity);
    }

    /**
     * {@code DELETE  /group-entities/:id} : delete the "id" groupEntity.
     *
     * @param id the id of the groupEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/group-entities/{id}")
    public ResponseEntity<Void> deleteGroupEntity(@PathVariable Long id) {
        log.debug("REST request to delete GroupEntity : {}", id);
        groupEntityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
