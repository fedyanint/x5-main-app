package ru.filit.x5.main.web.rest;

import ru.filit.x5.main.domain.ProductEntity;
import ru.filit.x5.main.service.ProductEntityService;
import ru.filit.x5.main.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.filit.x5.main.domain.ProductEntity}.
 */
@RestController
@RequestMapping("/api")
public class ProductEntityResource {

    private final Logger log = LoggerFactory.getLogger(ProductEntityResource.class);

    private static final String ENTITY_NAME = "mainProductEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductEntityService productEntityService;

    public ProductEntityResource(ProductEntityService productEntityService) {
        this.productEntityService = productEntityService;
    }

    /**
     * {@code POST  /product-entities} : Create a new productEntity.
     *
     * @param productEntity the productEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productEntity, or with status {@code 400 (Bad Request)} if the productEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-entities")
    public ResponseEntity<ProductEntity> createProductEntity(@RequestBody ProductEntity productEntity) throws URISyntaxException {
        log.debug("REST request to save ProductEntity : {}", productEntity);
        if (productEntity.getId() != null) {
            throw new BadRequestAlertException("A new productEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductEntity result = productEntityService.save(productEntity);
        return ResponseEntity.created(new URI("/api/product-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-entities} : Updates an existing productEntity.
     *
     * @param productEntity the productEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productEntity,
     * or with status {@code 400 (Bad Request)} if the productEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-entities")
    public ResponseEntity<ProductEntity> updateProductEntity(@RequestBody ProductEntity productEntity) throws URISyntaxException {
        log.debug("REST request to update ProductEntity : {}", productEntity);
        if (productEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductEntity result = productEntityService.save(productEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-entities} : get all the productEntities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productEntities in body.
     */
    @GetMapping("/product-entities")
    public ResponseEntity<List<ProductEntity>> getAllProductEntities(Pageable pageable) {
        log.debug("REST request to get a page of ProductEntities");
        Page<ProductEntity> page = productEntityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /product-entities/:id} : get the "id" productEntity.
     *
     * @param id the id of the productEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-entities/{id}")
    public ResponseEntity<ProductEntity> getProductEntity(@PathVariable Long id) {
        log.debug("REST request to get ProductEntity : {}", id);
        Optional<ProductEntity> productEntity = productEntityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productEntity);
    }

    /**
     * {@code DELETE  /product-entities/:id} : delete the "id" productEntity.
     *
     * @param id the id of the productEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-entities/{id}")
    public ResponseEntity<Void> deleteProductEntity(@PathVariable Long id) {
        log.debug("REST request to delete ProductEntity : {}", id);
        productEntityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
