/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.filit.x5.main.web.rest.vm;
