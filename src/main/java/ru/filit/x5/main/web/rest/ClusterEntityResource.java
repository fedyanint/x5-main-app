package ru.filit.x5.main.web.rest;

import ru.filit.x5.main.domain.ClusterEntity;
import ru.filit.x5.main.service.ClusterEntityService;
import ru.filit.x5.main.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.filit.x5.main.domain.ClusterEntity}.
 */
@RestController
@RequestMapping("/api")
public class ClusterEntityResource {

    private final Logger log = LoggerFactory.getLogger(ClusterEntityResource.class);

    private static final String ENTITY_NAME = "mainClusterEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClusterEntityService clusterEntityService;

    public ClusterEntityResource(ClusterEntityService clusterEntityService) {
        this.clusterEntityService = clusterEntityService;
    }

    /**
     * {@code POST  /cluster-entities} : Create a new clusterEntity.
     *
     * @param clusterEntity the clusterEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clusterEntity, or with status {@code 400 (Bad Request)} if the clusterEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cluster-entities")
    public ResponseEntity<ClusterEntity> createClusterEntity(@RequestBody ClusterEntity clusterEntity) throws URISyntaxException {
        log.debug("REST request to save ClusterEntity : {}", clusterEntity);
        if (clusterEntity.getId() != null) {
            throw new BadRequestAlertException("A new clusterEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClusterEntity result = clusterEntityService.save(clusterEntity);
        return ResponseEntity.created(new URI("/api/cluster-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cluster-entities} : Updates an existing clusterEntity.
     *
     * @param clusterEntity the clusterEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clusterEntity,
     * or with status {@code 400 (Bad Request)} if the clusterEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clusterEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cluster-entities")
    public ResponseEntity<ClusterEntity> updateClusterEntity(@RequestBody ClusterEntity clusterEntity) throws URISyntaxException {
        log.debug("REST request to update ClusterEntity : {}", clusterEntity);
        if (clusterEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClusterEntity result = clusterEntityService.save(clusterEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clusterEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cluster-entities} : get all the clusterEntities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clusterEntities in body.
     */
    @GetMapping("/cluster-entities")
    public ResponseEntity<List<ClusterEntity>> getAllClusterEntities(Pageable pageable) {
        log.debug("REST request to get a page of ClusterEntities");
        Page<ClusterEntity> page = clusterEntityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cluster-entities/:id} : get the "id" clusterEntity.
     *
     * @param id the id of the clusterEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clusterEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cluster-entities/{id}")
    public ResponseEntity<ClusterEntity> getClusterEntity(@PathVariable Long id) {
        log.debug("REST request to get ClusterEntity : {}", id);
        Optional<ClusterEntity> clusterEntity = clusterEntityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clusterEntity);
    }

    /**
     * {@code DELETE  /cluster-entities/:id} : delete the "id" clusterEntity.
     *
     * @param id the id of the clusterEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cluster-entities/{id}")
    public ResponseEntity<Void> deleteClusterEntity(@PathVariable Long id) {
        log.debug("REST request to delete ClusterEntity : {}", id);
        clusterEntityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
