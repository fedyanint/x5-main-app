package ru.filit.x5.main.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A GroupEntity.
 */
@Entity
@Table(name = "group_entity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GroupEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "group")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ProductEntity> productEntitySets = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("groupEntitySets")
    private ClusterEntity cluster;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public GroupEntity name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ProductEntity> getProductEntitySets() {
        return productEntitySets;
    }

    public GroupEntity productEntitySets(Set<ProductEntity> productEntities) {
        this.productEntitySets = productEntities;
        return this;
    }

    public GroupEntity addProductEntitySet(ProductEntity productEntity) {
        this.productEntitySets.add(productEntity);
        productEntity.setGroup(this);
        return this;
    }

    public GroupEntity removeProductEntitySet(ProductEntity productEntity) {
        this.productEntitySets.remove(productEntity);
        productEntity.setGroup(null);
        return this;
    }

    public void setProductEntitySets(Set<ProductEntity> productEntities) {
        this.productEntitySets = productEntities;
    }

    public ClusterEntity getCluster() {
        return cluster;
    }

    public GroupEntity cluster(ClusterEntity clusterEntity) {
        this.cluster = clusterEntity;
        return this;
    }

    public void setCluster(ClusterEntity clusterEntity) {
        this.cluster = clusterEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroupEntity)) {
            return false;
        }
        return id != null && id.equals(((GroupEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "GroupEntity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
