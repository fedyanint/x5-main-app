package ru.filit.x5.main.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ClusterEntity.
 */
@Entity
@Table(name = "cluster_entity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClusterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "cluster")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<GroupEntity> groupEntitySets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClusterEntity name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<GroupEntity> getGroupEntitySets() {
        return groupEntitySets;
    }

    public ClusterEntity groupEntitySets(Set<GroupEntity> groupEntities) {
        this.groupEntitySets = groupEntities;
        return this;
    }

    public ClusterEntity addGroupEntitySet(GroupEntity groupEntity) {
        this.groupEntitySets.add(groupEntity);
        groupEntity.setCluster(this);
        return this;
    }

    public ClusterEntity removeGroupEntitySet(GroupEntity groupEntity) {
        this.groupEntitySets.remove(groupEntity);
        groupEntity.setCluster(null);
        return this;
    }

    public void setGroupEntitySets(Set<GroupEntity> groupEntities) {
        this.groupEntitySets = groupEntities;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClusterEntity)) {
            return false;
        }
        return id != null && id.equals(((ClusterEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ClusterEntity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
