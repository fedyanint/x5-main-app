package ru.filit.x5.main.web.rest;

import ru.filit.x5.main.MainApp;
import ru.filit.x5.main.config.SecurityBeanOverrideConfiguration;
import ru.filit.x5.main.domain.ProductEntity;
import ru.filit.x5.main.repository.ProductEntityRepository;
import ru.filit.x5.main.service.ProductEntityService;
import ru.filit.x5.main.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.filit.x5.main.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductEntityResource} REST controller.
 */
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, MainApp.class})
public class ProductEntityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ProductEntityRepository productEntityRepository;

    @Autowired
    private ProductEntityService productEntityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProductEntityMockMvc;

    private ProductEntity productEntity;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProductEntityResource productEntityResource = new ProductEntityResource(productEntityService);
        this.restProductEntityMockMvc = MockMvcBuilders.standaloneSetup(productEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductEntity createEntity(EntityManager em) {
        ProductEntity productEntity = new ProductEntity()
            .name(DEFAULT_NAME);
        return productEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductEntity createUpdatedEntity(EntityManager em) {
        ProductEntity productEntity = new ProductEntity()
            .name(UPDATED_NAME);
        return productEntity;
    }

    @BeforeEach
    public void initTest() {
        productEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductEntity() throws Exception {
        int databaseSizeBeforeCreate = productEntityRepository.findAll().size();

        // Create the ProductEntity
        restProductEntityMockMvc.perform(post("/api/product-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isCreated());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ProductEntity testProductEntity = productEntityList.get(productEntityList.size() - 1);
        assertThat(testProductEntity.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createProductEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productEntityRepository.findAll().size();

        // Create the ProductEntity with an existing ID
        productEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductEntityMockMvc.perform(post("/api/product-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProductEntities() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        // Get all the productEntityList
        restProductEntityMockMvc.perform(get("/api/product-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getProductEntity() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        // Get the productEntity
        restProductEntityMockMvc.perform(get("/api/product-entities/{id}", productEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingProductEntity() throws Exception {
        // Get the productEntity
        restProductEntityMockMvc.perform(get("/api/product-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductEntity() throws Exception {
        // Initialize the database
        productEntityService.save(productEntity);

        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();

        // Update the productEntity
        ProductEntity updatedProductEntity = productEntityRepository.findById(productEntity.getId()).get();
        // Disconnect from session so that the updates on updatedProductEntity are not directly saved in db
        em.detach(updatedProductEntity);
        updatedProductEntity
            .name(UPDATED_NAME);

        restProductEntityMockMvc.perform(put("/api/product-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductEntity)))
            .andExpect(status().isOk());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
        ProductEntity testProductEntity = productEntityList.get(productEntityList.size() - 1);
        assertThat(testProductEntity.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();

        // Create the ProductEntity

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductEntityMockMvc.perform(put("/api/product-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductEntity() throws Exception {
        // Initialize the database
        productEntityService.save(productEntity);

        int databaseSizeBeforeDelete = productEntityRepository.findAll().size();

        // Delete the productEntity
        restProductEntityMockMvc.perform(delete("/api/product-entities/{id}", productEntity.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
