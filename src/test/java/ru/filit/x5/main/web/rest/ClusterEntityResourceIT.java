package ru.filit.x5.main.web.rest;

import ru.filit.x5.main.MainApp;
import ru.filit.x5.main.config.SecurityBeanOverrideConfiguration;
import ru.filit.x5.main.domain.ClusterEntity;
import ru.filit.x5.main.repository.ClusterEntityRepository;
import ru.filit.x5.main.service.ClusterEntityService;
import ru.filit.x5.main.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.filit.x5.main.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ClusterEntityResource} REST controller.
 */
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, MainApp.class})
public class ClusterEntityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ClusterEntityRepository clusterEntityRepository;

    @Autowired
    private ClusterEntityService clusterEntityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClusterEntityMockMvc;

    private ClusterEntity clusterEntity;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClusterEntityResource clusterEntityResource = new ClusterEntityResource(clusterEntityService);
        this.restClusterEntityMockMvc = MockMvcBuilders.standaloneSetup(clusterEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClusterEntity createEntity(EntityManager em) {
        ClusterEntity clusterEntity = new ClusterEntity()
            .name(DEFAULT_NAME);
        return clusterEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClusterEntity createUpdatedEntity(EntityManager em) {
        ClusterEntity clusterEntity = new ClusterEntity()
            .name(UPDATED_NAME);
        return clusterEntity;
    }

    @BeforeEach
    public void initTest() {
        clusterEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createClusterEntity() throws Exception {
        int databaseSizeBeforeCreate = clusterEntityRepository.findAll().size();

        // Create the ClusterEntity
        restClusterEntityMockMvc.perform(post("/api/cluster-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clusterEntity)))
            .andExpect(status().isCreated());

        // Validate the ClusterEntity in the database
        List<ClusterEntity> clusterEntityList = clusterEntityRepository.findAll();
        assertThat(clusterEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ClusterEntity testClusterEntity = clusterEntityList.get(clusterEntityList.size() - 1);
        assertThat(testClusterEntity.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createClusterEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clusterEntityRepository.findAll().size();

        // Create the ClusterEntity with an existing ID
        clusterEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClusterEntityMockMvc.perform(post("/api/cluster-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clusterEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ClusterEntity in the database
        List<ClusterEntity> clusterEntityList = clusterEntityRepository.findAll();
        assertThat(clusterEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllClusterEntities() throws Exception {
        // Initialize the database
        clusterEntityRepository.saveAndFlush(clusterEntity);

        // Get all the clusterEntityList
        restClusterEntityMockMvc.perform(get("/api/cluster-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clusterEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getClusterEntity() throws Exception {
        // Initialize the database
        clusterEntityRepository.saveAndFlush(clusterEntity);

        // Get the clusterEntity
        restClusterEntityMockMvc.perform(get("/api/cluster-entities/{id}", clusterEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(clusterEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingClusterEntity() throws Exception {
        // Get the clusterEntity
        restClusterEntityMockMvc.perform(get("/api/cluster-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClusterEntity() throws Exception {
        // Initialize the database
        clusterEntityService.save(clusterEntity);

        int databaseSizeBeforeUpdate = clusterEntityRepository.findAll().size();

        // Update the clusterEntity
        ClusterEntity updatedClusterEntity = clusterEntityRepository.findById(clusterEntity.getId()).get();
        // Disconnect from session so that the updates on updatedClusterEntity are not directly saved in db
        em.detach(updatedClusterEntity);
        updatedClusterEntity
            .name(UPDATED_NAME);

        restClusterEntityMockMvc.perform(put("/api/cluster-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedClusterEntity)))
            .andExpect(status().isOk());

        // Validate the ClusterEntity in the database
        List<ClusterEntity> clusterEntityList = clusterEntityRepository.findAll();
        assertThat(clusterEntityList).hasSize(databaseSizeBeforeUpdate);
        ClusterEntity testClusterEntity = clusterEntityList.get(clusterEntityList.size() - 1);
        assertThat(testClusterEntity.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingClusterEntity() throws Exception {
        int databaseSizeBeforeUpdate = clusterEntityRepository.findAll().size();

        // Create the ClusterEntity

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClusterEntityMockMvc.perform(put("/api/cluster-entities")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clusterEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ClusterEntity in the database
        List<ClusterEntity> clusterEntityList = clusterEntityRepository.findAll();
        assertThat(clusterEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClusterEntity() throws Exception {
        // Initialize the database
        clusterEntityService.save(clusterEntity);

        int databaseSizeBeforeDelete = clusterEntityRepository.findAll().size();

        // Delete the clusterEntity
        restClusterEntityMockMvc.perform(delete("/api/cluster-entities/{id}", clusterEntity.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClusterEntity> clusterEntityList = clusterEntityRepository.findAll();
        assertThat(clusterEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
