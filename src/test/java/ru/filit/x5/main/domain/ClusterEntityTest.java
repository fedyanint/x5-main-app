package ru.filit.x5.main.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ru.filit.x5.main.web.rest.TestUtil;

public class ClusterEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClusterEntity.class);
        ClusterEntity clusterEntity1 = new ClusterEntity();
        clusterEntity1.setId(1L);
        ClusterEntity clusterEntity2 = new ClusterEntity();
        clusterEntity2.setId(clusterEntity1.getId());
        assertThat(clusterEntity1).isEqualTo(clusterEntity2);
        clusterEntity2.setId(2L);
        assertThat(clusterEntity1).isNotEqualTo(clusterEntity2);
        clusterEntity1.setId(null);
        assertThat(clusterEntity1).isNotEqualTo(clusterEntity2);
    }
}
